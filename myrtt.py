import os, sys
import commands
import logging
import time
import json
import datetime
import errno
import glob
import urllib, urllib2
from optparse import OptionParser

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

list_of_releases = ['20.7.X.Y-VAL','21.0.X.Y-VAL']

release_aware_stuff = {
'20.7.X.Y-VAL' : {
    'InputDataFile' : "root://eosatlas//eos/atlas/user/m/mdobre/forRTTdata15/Data16_207.AOD.pool.root",
        'InputMCFile' : "root://eosatlas//eos/atlas/user/m/mdobre/forRTTmc15/MC15_207.AOD.pool.root",
        'DerivationsListFile' : "R20_DerivList.json",
        'DataYear' : '16',
        'MCYear' : '15',
        'Output_json_file_sufix' : 'r20',
        'Release_Sufix' : ''},
    '21.0.X.Y-VAL' : {
        'InputDataFile' : "root://eosatlas//eos/atlas/user/m/mdobre/forRTTdata16/Data16_210.AOD.pool.root",
        'InputMCFile' : "root://eosatlas//eos/atlas/user/m/mdobre/forRTTmc16/MC16_210.AOD.pool.root",
        'DerivationsListFile' : "R21_DerivList.json",
        'DataYear' : '16',
        'MCYear' : '16',
        'Output_json_file_sufix' : 'r21',
        'Release_Sufix' : '_210XY'}} 
 
class myRTT():

  def __init__(self, _release, logger=None):
    self.logger = logger or logging.getLogger(__name__)
    # checkxAOD /eos/atlas/atlascerngroupdisk/proj-sit/rtt/prod/rtt/rel_1/20.7.X.Y-VAL/x86_64-slc6-gcc49-opt/AtlasDerivation/DerivationFrameworkRTT/Data16JETM1/DAOD_JETM1.Data16.pool.root
    self.checkxAODCommand = "checkxAOD"
    self.basePath = "/eos/atlas/atlascerngroupdisk/proj-sit/rtt/prod/rtt/"
    self.basePathAFS = "/afs/cern.ch/atlas/project/RTT/prod/Results/rtt/"
    self.nightlyVer = "rel_0"
    self.release = _release
    self.cmtConfig = "x86_64-slc6-gcc49-opt"
    self.rttName = "AtlasDerivation/DerivationFrameworkRTT"
    self.derivationName = "Data16JETM1"
    self.xAODName = "DAOD_JETM1.Data16.pool.root"

    # performance log URL
    # http://atlas-rtt.cern.ch/prod/rtt/rel_2/20.7.X.Y-VAL/build/x86_64-slc6-gcc49-opt/AtlasDerivation/DerivationFrameworkRTT/Data16JETM11/DerivationSummary_Data16JETM11.txt
    self.baseLogURL = "http://atlas-rtt.cern.ch/prod/rtt/"
    self.baseLogBuild = "build"
    self.LogName = "DerivationSummary_Data16JETM11.txt"

    # curl command for elastic search
    # !!!! USER/PASS should not be stored here !!!!
    self.posttoelastic = True
    self.curlcmd1 = """curl --capath /etc/grid-security/certificates -u '%s:%s' -XPUT https://es-atlas.cern.ch:9203/atlas_rtt-%s -d '{
    "mappings": { "report": { "properties": {
                "input": {
                    "properties": {
                        "fileName": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "derivation": {
                            "type": "string",
                            "index": "not_analyzed"}}},
                "output": {
                    "properties": {
                        "fileName": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "derivation": {
                            "type": "string",
                            "index": "not_analyzed"}}}}}}}'"""

    self.curlcmd2 = """curl --capath /etc/grid-security/certificates -u '%s:%s' -XPOST https://es-atlas.cern.ch:9203/atlas_rtt/report -d @%s"""

    # Input datafiles
    self.inputDataFile = release_aware_stuff[_release]['InputDataFile']
    self.inputMCFile = release_aware_stuff[_release]['InputMCFile']

    # Output datafiles
    with open(release_aware_stuff[self.release]['DerivationsListFile']) as data_file:    
      self.derivationDict = json.load(data_file)

    # Output datafiles for testing
    self.derivationDict2 = { 
      "Data16JETM1" : "DAOD_JETM1.Data16.pool.root",
      "MC15JETM1"   : "DAOD_JETM1.MC15.pool.root",
      }

    return

  def _urlType(self,filename):
    if filename.startswith('dcap:'):
      return 'dcap'
    if filename.startswith('root:'):
      return 'root'
    if filename.startswith('https:'):
      return 'https'
    if filename.startswith('rfio:'):
      return 'rfio'
    if filename.startswith('file:'):
      return 'posix'
    return 'posix'

  def _get_file_size(self,filename):
    if filename.startswith("/eos"):
      filename = "root://eosatlas/" + filename

    if self._urlType(filename) == 'posix':
      try:
        fsize = os.stat(filename)[6]
      except:
        fsize = 0
    else:
      import ROOT
      try:
        self.logger.debug('Calling TFile.Open for {0}'.format(filename))
        pos = filename.find("?")
        if pos>=0:
          extraparam = '&filetype=raw'
        else:
          extraparam = '?filetype=raw'

        file = ROOT.TFile.Open(filename + extraparam, 'READ')
        fsize = file.GetSize()
        self.logger.debug('Got size {0} from TFile.GetSize'.format(fsize))
      except ReferenceError:
        self.logger.warning('Failed to get size of {0}'.format(filename))
        fsize = 0
      try:
        file.Close()
      except:
        pass
      del ROOT
    return fsize

  def downloadLogs(self):
    # IMPORTANT: reusing here self.nightlyVer from checkxAOD run

    # Create output dir to store json files
    self.logger.debug("If not does not exist, creating output directory log_%s" %self.nightlyVer)
    path = "log_%s" %self.nightlyVer
    try:
      os.makedirs(path)
    except OSError as exception:
      if exception.errno != errno.EEXIST:
        raise
    # Remove old json files
    oldFiles = glob.glob(os.path.join(path,'*.txt'))
    self.logger.debug("Removing old files in %s" % oldFiles)
    for f in oldFiles:
      os.remove(f)

    for deriv, name in self.derivationDict.iteritems():
      logName = "DerivationSummary_%s.txt" % deriv
      logURL = os.path.join(self.baseLogURL, self.nightlyVer, self.release, self.baseLogBuild, self.cmtConfig, self.rttName, deriv, logName )
      self.logger.debug("Downloading %s" %logURL)
      cmd = "wget %s --directory-prefix=%s" % (logURL, path)
      rc, out = commands.getstatusoutput(cmd)
      if rc:
        logger.warning("%s, %s" %(rc, out))

    return

  def parseLogs(self):
    infosDict = {}

    # IMPORTANT: reusing here self.nightlyVer from checkxAOD run
    for deriv, name in self.derivationDict.iteritems():
      logName = "DerivationSummary_%s.txt" % deriv
      logURL = os.path.join(self.baseLogURL, self.nightlyVer, self.release, self.baseLogBuild, self.cmtConfig, self.rttName, deriv, logName )
      self.logger.debug("Downloading %s" %logURL)

      f = urllib.urlopen(logURL)
      myLog = f.read()
      lines = myLog.split("\n")
      for count, line in enumerate(lines):
        if "Car" in line:
          categories = lines[count:count+2]

      categoryDict = {}
      try:
        keys = categories[0].split()
        values = categories[1].split()
      except:
        keys = []
        values = []

      try:
        # Store CPU time
        categoryDict[keys[4]+"_seconds"] = float(values[4])/1000.
        # Store VMEM
        categoryDict[keys[5]+"_bytes"] = int(round(float(values[6])*1024*1024))
        # Store MemLeak
        categoryDict[keys[6]+"_bytes"] = int(round(float(values[8])*1024))
        # TotalJobTime(Chronostat)
        #categoryDict[keys[8]] = int(round(float(values[11])*60.))
      except:
        pass

      infosDict[name] = categoryDict

    return infosDict

  def parseJobReport(self):
    infosDict = {}
    # IMPORTANT: reusing here self.nightlyVer from checkxAOD run
    
    for deriv, name in self.derivationDict.iteritems():
      jobReportName = "jobReport.json"
      logURL = os.path.join(self.baseLogURL, self.nightlyVer, self.release, self.baseLogBuild, self.cmtConfig, self.rttName, deriv, jobReportName )
      self.logger.debug("Downloading %s" %logURL)

      f = urllib.urlopen(logURL)
      try:
        data = json.loads(f.read())
      except:
        data = {}

      categoryDict = {}
      try:
        # Store maxPSS
        categoryDict["maxPSS_bytes"] = data['resource']['executor']['AODtoDAOD']['memory']['Max']['maxPSS']*1024
        # Store avgPSS
        categoryDict["avgPSS_bytes"] = data['resource']['executor']['AODtoDAOD']['memory']['Avg']['avgPSS']*1024
        # cpuEfficiency
        categoryDict["cpuEfficiency"] = data['resource']['transform']['cpuEfficiency']
        # cpuPWEfficiency
        categoryDict["cpuPWEfficiency"] = data['resource']['transform']['cpuPWEfficiency']
        # cpuTimeTotal
        categoryDict["cpuTimeTotal_seconds"] = data['resource']['transform']['cpuTimeTotal']
        # wallTime
        categoryDict["wallTime_seconds"] = data['resource']['transform']['wallTime']
      except:
        pass

      infosDict[name] = categoryDict

    return infosDict

  def parseInfo(self, info):
    events = 0
    categories = []
    lines = info.split("\n")
    for count, line in enumerate(lines):
      if "CSV" in line:
        categories = lines[count+1:count+3]
      if (events == 0) and ("Total" in line):
        try:
          events = line.split()[7]
        except:
          events = 0

    categoryDict = {}
    if not categories:
      return categoryDict

    keys = categories[0].split(",")
    values = categories[1].split(",")
    for key, value in zip(keys, values):
      if key == "*Unknown*":
        key = key.replace("*Unknown*", "Unknown")

      categoryDict[key + "_bytes"] = int(round(float(value)*1024))
    
    categoryDict["events"] = int(events)

    return categoryDict

  def getFileSize(self, fileName):
    fileSize = 0
    turlStart = "root://eosatlas/"
    if fileName.startswith(turlStart):
      fileName = fileName.replace(turlStart, "")
    cmd = "eos ls -al " + fileName
    rc, out = commands.getstatusoutput(cmd)
    if rc:
      logger.warning("%s, %s" %(rc, out))

    try:
      fileSize = out.split()[4]
    except:
      fileSize = 0
    
    return fileSize

  def getFileInfo(self, fileName, deriv=""):
    # Use webserver file first
    jobReportName = "outf_checkxAOD_2.txt"
    logURL = os.path.join(self.baseLogURL, self.nightlyVer, self.release, self.baseLogBuild, self.cmtConfig, self.rttName, deriv, jobReportName )
    self.logger.debug("Downloading %s" %logURL)

    try:
      f = urllib2.urlopen(logURL)
      out = f.read()
      rc = 0
    except urllib2.HTTPError, e:
      self.logger.error( e )
      self.logger.error( "Fail-over to disk" )
      # Fail-over to disk
      cmd = self.checkxAODCommand + " " + fileName
      self.logger.debug("Command to execute:  %s" % cmd)
      rc, out = commands.getstatusoutput(cmd)
      if rc:
        self.logger.warning("%s, %s" %(rc, out))

    return rc, out

  def combineInfo(self, fileName, deriv):
    contentInfo = {}
    path, fileShort = os.path.split(fileName)
    
    contentInfo["fileName"] = fileShort
    contentInfo["timeStamp"] = datetime.datetime.utcnow().isoformat()
    contentInfo["release"] = self.release
    # get file size 
    #fileSize = self.getFileSize(fileName)
    fileSize = self._get_file_size(fileName)
    contentInfo["bytes"] = int(fileSize)

    # get info from checkxAOD 
    rc, checkxAODInfo = self.getFileInfo(fileName, deriv)
    if rc:
      # something went wrong
      return contentInfo
  
    # parse text from checkxAOD - returns a dict
    contentInfoNew = self.parseInfo(checkxAODInfo)
    # merge the dictionaries
    contentInfo = dict(contentInfo.items() + contentInfoNew.items())
    # get file size again
    #fileSize = self.getFileSize(fileName)
    fileSize = self._get_file_size(fileName)
    contentInfo["bytes"] = int(fileSize)

    return contentInfo

  def getInputFileInfo(self):

    info = {}
    # Data16
    fileName = self.inputDataFile
    contentInfo = self.combineInfo(fileName, "")
    path, fileShort = os.path.split(fileName)
    contentInfo['derivation'] = 'InputData' + release_aware_stuff[self.release]['DataYear']  
    info[fileShort] = contentInfo

    # MC15
    fileName = self.inputMCFile
    contentInfo = self.combineInfo(fileName, "")
    path, fileShort = os.path.split(fileName)
    contentInfo['derivation'] = 'InputMC' + release_aware_stuff[self.release]['MCYear']   
    info[fileShort] = contentInfo

    return info

  def getOutputFileInfo(self):

    derivationName = self.derivatioName
    xAODName = self.xAODName

    filePathName = os.path.join(self.basePath, self.nightlyVer, self.release, self.cmtConfig, self.rttName, derivationName, xAODName)
    contentInfo = self.combineInfo(filePathName, derivationName)
    contentInfo['derivation'] = derivationName

    path, filename = os.path.split(filePathName)
 
    return {filename : contentInfo }

  def getOutputFileInfoAll(self):

    allInfo = {}
    # determine rel_X
    rel = datetime.datetime.today().weekday() + 1
    if rel == 7:
      rel = 0
    relString = "rel_" + str(rel)
    self.logger.debug("Found %s" %relString )

    self.logger.debug("Setting %s" %relString )
    self.nightlyVer = relString

    # Process all data
    for deriv, name in self.derivationDict.iteritems():
      filePathName = os.path.join(self.basePath, relString, self.release, self.cmtConfig, self.rttName, deriv, name)
      self.logger.info("Processing: %s " % filePathName)
      # First check if file exist by checking fileSize
      fileSize = self._get_file_size(filePathName)
      # Fail-over from EOS to AFS
      if (fileSize==0):
        filePathName = os.path.join(self.basePathAFS, relString, self.release, "build", self.cmtConfig, self.rttName, deriv, name)
        self.logger.info("Processing fail-over location: %s " % filePathName)

      contentInfo = self.combineInfo(filePathName, deriv)
      contentInfo['derivation'] = deriv
      path, filename = os.path.split(filePathName)
      allInfo[filename] = contentInfo

    return allInfo

  def postToElastic(self, info):
    self.logger.debug("Prepare for elasticsearch")
    #self.logger.debug(info)

    # Create output dir to store json files
    self.logger.debug("If not does not exist, creating output directory %s" %self.nightlyVer)
    path = self.nightlyVer
    try:
      os.makedirs(path)
    except OSError as exception:
      if exception.errno != errno.EEXIST:
        raise
    # Remove old json files
    oldFiles = glob.glob(os.path.join(path,'*' + release_aware_stuff[self.release]['Output_json_file_sufix'] + '*.json'))
    self.logger.debug("Removing old files in %s" % oldFiles)
    for f in oldFiles:
      os.remove(f)

    # First find inputData and inputMC
    inputData = {}
    inputMC = {}
    for fileName, content in info.iteritems():
      if content['derivation'] == 'InputData' + release_aware_stuff[self.release]['DataYear']:
        inputData = content
      if content['derivation'] == 'InputMC'  + release_aware_stuff[self.release]['MCYear']:
        inputMC = content

    # Get credentials
    try:
      from secure import user
      from secure import passwd
    except:
      user = "abc"
      passwd = "abc"
      self.logger.error("User name and password for elasticsearch are missing")

    # Create index mapping for the new day
    todayDate = datetime.datetime.utcnow().date().isoformat()
    #cmd = self.curlcmd1 % (user, passwd, todayDate)
    #if self.posttoelastic:
    #  rc, out = commands.getstatusoutput(cmd)
    #else:
    #  rc = 0
    #  out = "Posting to elastic search switched off"
    #if rc:
    #  self.logger.warning("New elastic search index mapping command failed with exit code: %s, %s" %(rc, out))
    #else:
    #  self.logger.debug("Index mapping: %s" %(out))

    
    # Now create input/output json files
    # Start with _1 as suffix of file name 
    num = 1

    #rel_suffix = release_aware_stuff[self.release]['Release_Sufix']
    #inputData['derivation'] = inputData['derivation'] + rel_suffix
    #inputMC['derivation'] = inputMC['derivation'] + rel_suffix

    for fileName, content in info.iteritems():
      out = {}

      #content['derivation'] = content['derivation'] + rel_suffix

      if content['derivation'].startswith("Data"):
        out['output'] = content
        out['input'] = inputData
      elif content['derivation'].startswith("MC"):
        out['input'] = inputMC
        out['output'] = content
      
      if out:
        self.logger.debug(out)
        filePathName = os.path.join(os.getcwd(), path, 'myrtt_' + release_aware_stuff[self.release]['Output_json_file_sufix'] + '_%s.json' %num)
        # write out file: 'w' write, 'a' append
        with open(filePathName , 'w') as outfile:
          json.dump(out, outfile)

        # Now post to elasticseach
        cmd = self.curlcmd2 %(user, passwd, filePathName)
        if self.posttoelastic:
          rc, out = commands.getstatusoutput(cmd)
        else:
          rc = 0
          out = "Posting to elastic search switched off"
        if rc:
          self.logger.warning("Post of %s to elasticsearch failed with exit code: %s, %s" %(filePathName, rc, out))
        else:
          self.logger.debug("%s: %s" %(filePathName, out))

        num=num+1

    return

  def postToElasticFile(self, nightlyVer = "rel_2"):
    self.logger.debug("Prepare for elasticsearch from json file")

    # Get credentials
    try:
      from secure import user
      from secure import passwd
    except:
      user = "abc"
      passwd = "abc"
      self.logger.error("User name and password for elasticsearch are missing")

    # Create index mapping for the new day
    todayDate = datetime.datetime.utcnow().date().isoformat()
    
    # Not needed anymore with new index
    #todayDate = datetime.date(2017, 1, 4).isoformat()
    #cmd = self.curlcmd1 % (user, passwd, todayDate)
    #self.logger.debug("Create index mapping for the new day")
    #rc, out = commands.getstatusoutput(cmd)
    #if rc:
    #  self.logger.warning("New elastic search index mapping command failed with exit code: %s, %s" %(rc, out))
    #else:
    #  self.logger.debug("Index mapping: %s" %(out))

    path = nightlyVer
    pathFiles = os.path.join(os.getcwd(), path,'*.json')
    jsonfiles = glob.glob(pathFiles)

    self.logger.debug("Now post individual json files from %s" %pathFiles)
    for jsonfile in jsonfiles:
      # Now post to elasticseach
      cmd = self.curlcmd2 %(user, passwd, jsonfile)
      rc, out = commands.getstatusoutput(cmd)
      if rc:
        self.logger.warning("Post of %s to elasticsearch failed with exit code: %s, %s" %(jsonfile, rc, out))
      else:
        self.logger.debug("%s: %s" %(jsonfile, out))

    return

  def run(self, argv):

    # Get fileinfo for a single static file
    inputInfo = self.getInputFileInfo()
    #self.logger.debug(inputInfo)

    # Get outfile info for single file
    #outputInfo = self.getOutputFileInfo()
    
    # Get outfile info for a dictionary of files
    outputInfo = self.getOutputFileInfoAll()
    #self.logger.debug(outputInfo)

    # Get additional infos from job logfiles
    logInfo = self.parseLogs()
    #self.logger.debug(logInfo)

    # Get additional infos from jobReport.json
    jobReportInfo = self.parseJobReport()
    #self.logger.debug(jobReportInfo)

    # Combined output info from checkxAOD, parsing job logfiles and jobReport.json
    combInfo = {}
    for key in outputInfo.iterkeys():
      item1 = outputInfo[key]
      item2 = logInfo[key]
      item3 = jobReportInfo[key]
      combInfo[key] = dict(item1.items()+item2.items()+item3.items())

    #self.logger.debug(combInfo)

    # Join input and output file dictionaries
    allInfo = dict(inputInfo.items() + combInfo.items())
    #self.logger.debug(allInfo)

    # Upload to elastic search
    self.postToElastic(allInfo)

    return

if __name__ == "__main__":

  parser=OptionParser()
  parser.add_option("-r","--release",action="store",dest="release",default='20.7.X.Y-VAL')
  (options,args)=parser.parse_args()

  if (options.release not in list_of_releases) :
    logger.error("Release [" + options.release + "] is not currently supported (releases supported : %s)" %(list_of_releases))
    sys.exit()

  app = myRTT(options.release)
  app.run(sys.argv)

  # Tests 
  #app.postToElasticFile("rel_2")
  #print(app.parseLogs())
  #print(app.parseJobReport())
