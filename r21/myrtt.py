import os, sys
import commands
import logging
import time
import json
import datetime
import errno
import glob
import urllib

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

class myRTT():

  def __init__(self, logger=None):
    self.logger = logger or logging.getLogger(__name__)
    # checkxAOD /eos/atlas/atlascerngroupdisk/proj-sit/rtt/prod/rtt/rel_1/20.7.X.Y-VAL/x86_64-slc6-gcc49-opt/AtlasDerivation/DerivationFrameworkRTT/Data16JETM1/DAOD_JETM1.Data16.pool.root
    self.checkxAODCommand = "checkxAOD"
    self.basePath = "/eos/atlas/atlascerngroupdisk/proj-sit/rtt/prod/rtt/"
    self.basePathAFS = "/afs/cern.ch/atlas/project/RTT/prod/Results/rtt/"
    self.nightlyVer = "rel_0"
    self.release = "21.0.X.Y-VAL" 
    self.cmtConfig = "x86_64-slc6-gcc49-opt"
    self.rttName = "AtlasDerivation/DerivationFrameworkRTT"
    self.derivationName = "Data16JETM1"
    self.xAODName = "DAOD_JETM1.Data16.pool.root"

    # performance log URL
    # http://atlas-rtt.cern.ch/prod/rtt/rel_2/20.7.X.Y-VAL/build/x86_64-slc6-gcc49-opt/AtlasDerivation/DerivationFrameworkRTT/Data16JETM11/DerivationSummary_Data16JETM11.txt
    self.baseLogURL = "http://atlas-rtt.cern.ch/prod/rtt/"
    self.baseLogBuild = "build"
    self.LogName = "DerivationSummary_Data16JETM11.txt"

    # curl command for elastic search
    # !!!! USER/PASS should not be stored here !!!!
    # FIXME
    self.posttoelastic = True
    self.curlcmd1 = """curl --capath /etc/grid-security/certificates -u '%s:%s' -XPUT https://es-atlas.cern.ch:9203/atlas_rtt-%s -d '{
    "mappings": { "report": { "properties": {
                "input": {
                    "properties": {
                        "fileName": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "derivation": {
                            "type": "string",
                            "index": "not_analyzed"}}},
                "output": {
                    "properties": {
                        "fileName": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "derivation": {
                            "type": "string",
                            "index": "not_analyzed"}}}}}}}'"""

    self.curlcmd2 = """curl --capath /etc/grid-security/certificates -u '%s:%s' -XPOST https://es-atlas.cern.ch:9203/atlas_rtt/report -d @%s"""

    # Input datafiles
    self.inputDataFile = "root://eosatlas//eos/atlas/user/m/mdobre/forRTTdata16/Data16_210.AOD.pool.root"
    self.inputMCFile = "root://eosatlas//eos/atlas/user/m/mdobre/forRTTmc16/MC16_210.AOD.pool.root"

    # Output datafiles
    # FIXME
    self.derivationDict = { 
      "Data16JETM1" : "DAOD_JETM1.Data16.pool.root",
      "Data16JETM2" : "DAOD_JETM2.Data16.pool.root",
      "Data16JETM3" : "DAOD_JETM3.Data16.pool.root",
      "Data16JETM4" : "DAOD_JETM4.Data16.pool.root",
      "Data16JETM5" : "DAOD_JETM5.Data16.pool.root",
      "Data16JETM6" : "DAOD_JETM6.Data16.pool.root",
      "Data16JETM7" : "DAOD_JETM7.Data16.pool.root",
      "Data16JETM8" : "DAOD_JETM8.Data16.pool.root",
      "Data16JETM9" : "DAOD_JETM9.Data16.pool.root",
      "Data16JETM10" : "DAOD_JETM10.Data16.pool.root",
      "Data16JETM11" : "DAOD_JETM11.Data16.pool.root",

      "MC16JETM1"   : "DAOD_JETM1.MC16.pool.root",
      "MC16JETM2"   : "DAOD_JETM2.MC16.pool.root",
      "MC16JETM3"   : "DAOD_JETM3.MC16.pool.root",
      "MC16JETM4"   : "DAOD_JETM4.MC16.pool.root",
      "MC16JETM5"   : "DAOD_JETM5.MC16.pool.root",
      "MC16JETM6"   : "DAOD_JETM6.MC16.pool.root",
      "MC16JETM7"   : "DAOD_JETM7.MC16.pool.root",
      "MC16JETM8"   : "DAOD_JETM8.MC16.pool.root",
      "MC16JETM9"   : "DAOD_JETM9.MC16.pool.root",
      "MC16JETM10"   : "DAOD_JETM10.MC16.pool.root",
      "MC16JETM11"   : "DAOD_JETM11.MC16.pool.root",

      "Data16SUSY1" : "DAOD_SUSY1.Data16.pool.root",
      "Data16SUSY2" : "DAOD_SUSY2.Data16.pool.root", 
      "Data16SUSY3" : "DAOD_SUSY3.Data16.pool.root", 
      "Data16SUSY4" : "DAOD_SUSY4.Data16.pool.root", 
      "Data16SUSY5" : "DAOD_SUSY5.Data16.pool.root", 
      "Data16SUSY6" : "DAOD_SUSY6.Data16.pool.root", 
      "Data16SUSY7" : "DAOD_SUSY7.Data16.pool.root", 
      "Data16SUSY8" : "DAOD_SUSY8.Data16.pool.root",
      "Data16SUSY10" : "DAOD_SUSY10.Data16.pool.root", 
      "Data16SUSY11" : "DAOD_SUSY11.Data16.pool.root", 
      "Data16SUSY12" : "DAOD_SUSY12.Data16.pool.root", 
      "Data16SUSY13" : "DAOD_SUSY13.Data16.pool.root", 
      "Data16SUSY14" : "DAOD_SUSY14.Data16.pool.root", 
      "Data16SUSY15" : "DAOD_SUSY15.Data16.pool.root", 
      "Data16SUSY16" : "DAOD_SUSY16.Data16.pool.root",
      
      "MC16SUSY1"   : "DAOD_SUSY1.MC16.pool.root", 
      "MC16SUSY2"   : "DAOD_SUSY2.MC16.pool.root", 
      "MC16SUSY3"   : "DAOD_SUSY3.MC16.pool.root", 
      "MC16SUSY4"   : "DAOD_SUSY4.MC16.pool.root", 
      "MC16SUSY5"   : "DAOD_SUSY5.MC16.pool.root", 
      "MC16SUSY6"   : "DAOD_SUSY6.MC16.pool.root", 
      "MC16SUSY7"   : "DAOD_SUSY7.MC16.pool.root", 
      "MC16SUSY8"   : "DAOD_SUSY8.MC16.pool.root", 
      "MC16SUSY10"   : "DAOD_SUSY10.MC16.pool.root", 
      "MC16SUSY11"   : "DAOD_SUSY11.MC16.pool.root", 
      "MC16SUSY12"   : "DAOD_SUSY12.MC16.pool.root", 
      "MC16SUSY13"   : "DAOD_SUSY13.MC16.pool.root", 
      "MC16SUSY14"   : "DAOD_SUSY14.MC16.pool.root", 
      "MC16SUSY15"   : "DAOD_SUSY15.MC16.pool.root", 
      "MC16SUSY16"   : "DAOD_SUSY16.MC16.pool.root", 
      "MC16SUSY17"   : "DAOD_SUSY17.MC16.pool.root",  
            
      }

    # FIXME
    self.derivationDict2 = { 
      "Data16JETM1" : "DAOD_JETM1.Data16.pool.root",
      "MC16JETM1"   : "DAOD_JETM1.MC16.pool.root",
      "Data16JETM2" : "DAOD_JETM2.Data16.pool.root",
      "MC15JETM2"   : "DAOD_JETM1.MC15.pool.root",
      }

    return

  def _urlType(self,filename):
    if filename.startswith('dcap:'):
      return 'dcap'
    if filename.startswith('root:'):
      return 'root'
    if filename.startswith('https:'):
      return 'https'
    if filename.startswith('rfio:'):
      return 'rfio'
    if filename.startswith('file:'):
      return 'posix'
    return 'posix'

  def _get_file_size(self,filename):
    if filename.startswith("/eos"):
      filename = "root://eosatlas/" + filename

    if self._urlType(filename) == 'posix':
      try:
        fsize = os.stat(filename)[6]
      except:
        fsize = 0
    else:
      import ROOT
      try:
        self.logger.debug('Calling TFile.Open for {0}'.format(filename))
        pos = filename.find("?")
        if pos>=0:
          extraparam = '&filetype=raw'
        else:
          extraparam = '?filetype=raw'

        file = ROOT.TFile.Open(filename + extraparam, 'READ')
        fsize = file.GetSize()
        self.logger.debug('Got size {0} from TFile.GetSize'.format(fsize))
      except ReferenceError:
        self.logger.warning('Failed to get size of {0}'.format(filename))
        fsize = 0
      try:
        file.Close()
      except:
        pass
      del ROOT
    return fsize

  def downloadLogs(self):
    # IMPORTANT: reusing here self.nightlyVer from checkxAOD run

    # Create output dir to store json files
    self.logger.debug("If not does not exist, creating output directory log_%s" %self.nightlyVer)
    path = "log_%s" %self.nightlyVer
    try:
      os.makedirs(path)
    except OSError as exception:
      if exception.errno != errno.EEXIST:
        raise
    # Remove old json files
    oldFiles = glob.glob(os.path.join(path,'*.txt'))
    self.logger.debug("Removing old files in %s" % oldFiles)
    for f in oldFiles:
      os.remove(f)

    for deriv, name in self.derivationDict.iteritems():
      logName = "DerivationSummary_%s.txt" % deriv
      logURL = os.path.join(self.baseLogURL, self.nightlyVer, self.release, self.baseLogBuild, self.cmtConfig, self.rttName, deriv, logName )
      self.logger.debug("Downloading %s" %logURL)
      cmd = "wget %s --directory-prefix=%s" % (logURL, path)
      rc, out = commands.getstatusoutput(cmd)
      if rc:
        logger.warning("%s, %s" %(rc, out))

    return

  def parseLogs(self):
    infosDict = {}

    # IMPORTANT: reusing here self.nightlyVer from checkxAOD run
    for deriv, name in self.derivationDict.iteritems():
      logName = "DerivationSummary_%s.txt" % deriv
      logURL = os.path.join(self.baseLogURL, self.nightlyVer, self.release, self.baseLogBuild, self.cmtConfig, self.rttName, deriv, logName )
      self.logger.debug("Downloading %s" %logURL)

      f = urllib.urlopen(logURL)
      myLog = f.read()
      lines = myLog.split("\n")
      for count, line in enumerate(lines):
        if "Car" in line:
          categories = lines[count:count+2]

      categoryDict = {}
      try:
        keys = categories[0].split()
        values = categories[1].split()
      except:
        keys = []
        values = []

      try:
        # Store CPU time
        categoryDict[keys[4]+"_seconds"] = float(values[4])/1000.
        # Store VMEM
        categoryDict[keys[5]+"_bytes"] = int(round(float(values[6])*1024*1024))
        # Store MemLeak
        categoryDict[keys[6]+"_bytes"] = int(round(float(values[8])*1024))
        # TotalJobTime(Chronostat)
        #categoryDict[keys[8]] = int(round(float(values[11])*60.))
      except:
        pass

      infosDict[name] = categoryDict

    return infosDict

  def parseJobReport(self):
    infosDict = {}
    # IMPORTANT: reusing here self.nightlyVer from checkxAOD run
    
    for deriv, name in self.derivationDict.iteritems():
      jobReportName = "jobReport.json"
      logURL = os.path.join(self.baseLogURL, self.nightlyVer, self.release, self.baseLogBuild, self.cmtConfig, self.rttName, deriv, jobReportName )
      self.logger.debug("Downloading %s" %logURL)

      f = urllib.urlopen(logURL)
      try:
        data = json.loads(f.read())
      except:
        data = {}

      categoryDict = {}
      try:
        # Store maxPSS
        categoryDict["maxPSS_bytes"] = data['resource']['executor']['AODtoDAOD']['memory']['Max']['maxPSS']*1024
        # Store avgPSS
        categoryDict["avgPSS_bytes"] = data['resource']['executor']['AODtoDAOD']['memory']['Avg']['avgPSS']*1024
        # cpuEfficiency
        categoryDict["cpuEfficiency"] = data['resource']['transform']['cpuEfficiency']
        # cpuPWEfficiency
        categoryDict["cpuPWEfficiency"] = data['resource']['transform']['cpuPWEfficiency']
        # cpuTimeTotal
        categoryDict["cpuTimeTotal_seconds"] = data['resource']['transform']['cpuTimeTotal']
        # wallTime
        categoryDict["wallTime_seconds"] = data['resource']['transform']['wallTime']
      except:
        pass

      infosDict[name] = categoryDict

    return infosDict

  def parseInfo(self, info):
    events = 0
    lines = info.split("\n")
    for count, line in enumerate(lines):
      if "CSV" in line:
        categories = lines[count+1:count+3]
      if (events == 0) and ("Total" in line):
        try:
          events = line.split()[7]
        except:
          events = 0

    categoryDict = {}
    keys = categories[0].split(",")
    values = categories[1].split(",")
    for key, value in zip(keys, values):
      if key == "*Unknown*":
        key = key.replace("*Unknown*", "Unknown")

      categoryDict[key + "_bytes"] = int(round(float(value)*1024))
    
    categoryDict["events"] = int(events)

    return categoryDict

  def getFileSize(self, fileName):
    fileSize = 0
    turlStart = "root://eosatlas/"
    if fileName.startswith(turlStart):
      fileName = fileName.replace(turlStart, "")
    cmd = "eos ls -al " + fileName
    rc, out = commands.getstatusoutput(cmd)
    if rc:
      logger.warning("%s, %s" %(rc, out))

    try:
      fileSize = out.split()[4]
    except:
      fileSize = 0
    
    return fileSize

  def getFileInfo(self, fileName):
    cmd = self.checkxAODCommand + " " + fileName
    self.logger.debug("Command to execute:  %s" % cmd)
    rc, out = commands.getstatusoutput(cmd)
    if rc:
      self.logger.warning("%s, %s" %(rc, out))

    return rc, out

  def combineInfo(self, fileName):
    contentInfo = {}
    path, fileShort = os.path.split(fileName)
    
    contentInfo["fileName"] = fileShort
    contentInfo["timeStamp"] = datetime.datetime.utcnow().isoformat()
    contentInfo["release"] = self.release
    # get file size 
    #fileSize = self.getFileSize(fileName)
    fileSize = self._get_file_size(fileName)
    contentInfo["bytes"] = int(fileSize)

    # get info from checkxAOD 
    rc, checkxAODInfo = self.getFileInfo(fileName)
    if rc:
      # something went wrong
      return contentInfo
  
    # parse text from checkxAOD - returns a dict
    contentInfoNew = self.parseInfo(checkxAODInfo)
    # merge the dictionaries
    contentInfo = dict(contentInfo.items() + contentInfoNew.items())
    # get file size again
    #fileSize = self.getFileSize(fileName)
    fileSize = self._get_file_size(fileName)
    contentInfo["bytes"] = int(fileSize)

    return contentInfo

  def getInputFileInfo(self):

    info = {}
    # Data16
    fileName = self.inputDataFile
    contentInfo = self.combineInfo(fileName)
    path, fileShort = os.path.split(fileName)
    contentInfo['derivation'] = 'InputData16' 
    info[fileShort] = contentInfo

    # MC16
    fileName = self.inputMCFile
    contentInfo = self.combineInfo(fileName)
    path, fileShort = os.path.split(fileName)
    contentInfo['derivation'] = 'InputMC16' 
    info[fileShort] = contentInfo

    return info

  def getOutputFileInfo(self):

    derivationName = self.derivatioName
    xAODName = self.xAODName

    filePathName = os.path.join(self.basePath, self.nightlyVer, self.release, self.cmtConfig, self.rttName, derivationName, xAODName)
    contentInfo = self.combineInfo(filePathName)
    contentInfo['derivation'] = derivationName

    path, filename = os.path.split(filePathName)
 
    return {filename : contentInfo }

  def getOutputFileInfoAll(self):

    allInfo = {}
    # determine rel_X
    rel = datetime.datetime.today().weekday() + 1
    if rel == 7:
      rel = 0
    relString = "rel_" + str(rel)
    # FIXME
    #relString = "rel_0"
    self.logger.debug("Found %s" %relString )

    self.logger.debug("Setting %s" %relString )
    self.nightlyVer = relString

    # Process all data
    for deriv, name in self.derivationDict.iteritems():
      filePathName = os.path.join(self.basePath, relString, self.release, self.cmtConfig, self.rttName, deriv, name)
      self.logger.info("Processing: %s " % filePathName)
      # First check if file exist by checking fileSize
      fileSize = self._get_file_size(filePathName)
      # Fail-over from EOS to AFS
      if (fileSize==0):
        filePathName = os.path.join(self.basePathAFS, relString, self.release, "build", self.cmtConfig, self.rttName, deriv, name)
        self.logger.info("Processing fail-over location: %s " % filePathName)

      contentInfo = self.combineInfo(filePathName)
      contentInfo['derivation'] = deriv
      path, filename = os.path.split(filePathName)
      allInfo[filename] = contentInfo

    return allInfo

  def postToElastic(self, info):
    self.logger.debug("Prepare for elasticsearch")
    #self.logger.debug(info)

    # Create output dir to store json files
    self.logger.debug("If not does not exist, creating output directory %s" %self.nightlyVer)
    path = self.nightlyVer
    try:
      os.makedirs(path)
    except OSError as exception:
      if exception.errno != errno.EEXIST:
        raise
    # Remove old json files
    oldFiles = glob.glob(os.path.join(path,'*.json'))
    self.logger.debug("Removing old files in %s" % oldFiles)
    for f in oldFiles:
      os.remove(f)

    # First find inputData and inputMC
    inputData = {}
    inputMC = {}
    for fileName, content in info.iteritems():
      if content['derivation'] == 'InputData16':
        inputData = content
      if content['derivation'] == 'InputMC16':
        inputMC = content

    # Get credentials
    try:
      from secure import user
      from secure import passwd
    except:
      user = "abc"
      passwd = "abc"
      self.logger.error("User name and password for elasticsearch are missing")

    # Create index mapping for the new day
    todayDate = datetime.datetime.utcnow().date().isoformat()
    # FIXME
    #todayDate = datetime.date(2017, 1, 22).isoformat()
    #cmd = self.curlcmd1 % (user, passwd, todayDate)
    #if self.posttoelastic:
    #  rc, out = commands.getstatusoutput(cmd)
    #else:
    #  rc = 0
    #  out = "Posting to elastic search switched off"
    #if rc:
    #  self.logger.warning("New elastic search index mapping command failed with exit code: %s, %s" %(rc, out))
    #else:
    #  self.logger.debug("Index mapping: %s" %(out))

    
    # Now create input/output json files
    # Start with _1 as suffix of file name 
    num = 1

    # FIXME - add _r210XY suffix to distiguish from 20.7.X.Y
    #rel_suffix = "_210XY"
    #inputData['derivation'] = inputData['derivation'] + rel_suffix
    #inputMC['derivation'] = inputMC['derivation'] + rel_suffix

    for fileName, content in info.iteritems():
      out = {}

      # FIXME - add _r210XY suffix to distiguish from 20.7.X.Y
      #if content['derivation'].startswith("Data16") or content['derivation'].startswith("MC16"): 
      #  content['derivation'] = content['derivation'] + rel_suffix

      if content['derivation'].startswith("Data16"):
        out['output'] = content
        out['input'] = inputData
      elif content['derivation'].startswith("MC16"):
        out['input'] = inputMC
        out['output'] = content
      
      if out:
        self.logger.debug(out)
        filePathName = os.path.join(os.getcwd(), path, 'myrtt_%s.json' %num)
        # write out file: 'w' write, 'a' append
        with open(filePathName , 'w') as outfile:
          json.dump(out, outfile)

        # Now post to elasticseach
        cmd = self.curlcmd2 %(user, passwd, filePathName)
        if self.posttoelastic:
          rc, out = commands.getstatusoutput(cmd)
        else:
          rc = 0
          out = "Posting to elastic search switched off"
        if rc:
          self.logger.warning("Post of %s to elasticsearch failed with exit code: %s, %s" %(filePathName, rc, out))
        else:
          self.logger.debug("%s: %s" %(filePathName, out))

        num=num+1

    return

  def postToElasticFile(self, nightlyVer = "rel_2"):
    self.logger.debug("Prepare for elasticsearch from json file")

    # Get credentials
    try:
      from secure import user
      from secure import passwd
    except:
      user = "abc"
      passwd = "abc"
      self.logger.error("User name and password for elasticsearch are missing")

    # Create index mapping for the new day
    todayDate = datetime.datetime.utcnow().date().isoformat()
    #todayDate = datetime.date(2017, 1, 21).isoformat()
    #cmd = self.curlcmd1 % (user, passwd, todayDate)
    #self.logger.debug("Create index mapping for the new day")
    #rc, out = commands.getstatusoutput(cmd)
    #if rc:
    #  self.logger.warning("New elastic search index mapping command failed with exit code: %s, %s" %(rc, out))
    #else:
    #  self.logger.debug("Index mapping: %s" %(out))

    path = nightlyVer
    pathFiles = os.path.join(os.getcwd(), path,'*.json')
    jsonfiles = glob.glob(pathFiles)

    self.logger.debug("Now post individual json files from %s" %pathFiles)
    for jsonfile in jsonfiles:
      # Now post to elasticseach
      cmd = self.curlcmd2 %(user, passwd, jsonfile)
      rc, out = commands.getstatusoutput(cmd)
      if rc:
        self.logger.warning("Post of %s to elasticsearch failed with exit code: %s, %s" %(jsonfile, rc, out))
      else:
        self.logger.debug("%s: %s" %(jsonfile, out))

    return

  def run(self, argv):

    # Get fileinfo for a single static file
    inputInfo = self.getInputFileInfo()
    #self.logger.debug(inputInfo)

    # Get outfile info for single file
    #outputInfo = self.getOutputFileInfo()
    
    # Get outfile info for a dictionary of files
    outputInfo = self.getOutputFileInfoAll()
    #self.logger.debug(outputInfo)

    # Get additional infos from job logfiles
    logInfo = self.parseLogs()
    #self.logger.debug(logInfo)

    # Get additional infos from jobReport.json
    jobReportInfo = self.parseJobReport()
    #self.logger.debug(jobReportInfo)

    # Combined output info from checkxAOD, parsing job logfiles and jobReport.json
    combInfo = {}
    for key in outputInfo.iterkeys():
      item1 = outputInfo[key]
      item2 = logInfo[key]
      item3 = jobReportInfo[key]
      combInfo[key] = dict(item1.items()+item2.items()+item3.items())

    #self.logger.debug(combInfo)

    # Join input and output file dictionaries
    allInfo = dict(inputInfo.items() + combInfo.items())
    #self.logger.debug(allInfo)

    # Upload to elastic search
    self.postToElastic(allInfo)

    return

if __name__ == "__main__":
  app = myRTT()
  app.run(sys.argv)

  # Tests 
  #app.postToElasticFile("rel_2")
  #print(app.parseLogs())
  #print(app.parseJobReport())
