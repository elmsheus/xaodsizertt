#!/bin/bash
# Run interactively on lxplus with:
# setupATLAS
# asetup AtlasOffline,21.0.12
# python myrtt.py

# directory for execution and storing json output 
DIR=/afs/cern.ch/user/e/elmsheus/xaodsizertt

cd $DIR
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh --quiet
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/AtlasSetup/current/AtlasSetup/scripts/asetup.sh AtlasOffline,21.0.12
python $DIR/myrtt.py -r '20.7.X.Y-VAL'
python $DIR/myrtt.py -r '21.0.X.Y-VAL'

